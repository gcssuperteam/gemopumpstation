﻿// Decompiled with JetBrains decompiler
// Type: dsRestorder
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

[HelpKeyword("vs.data.DataSet")]
[XmlRoot("dsRestorder")]
[DesignerCategory("code")]
[ToolboxItem(true)]
[GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
[XmlSchemaProvider("GetTypedDataSetSchema")]
[Serializable]
public class dsRestorder : DataSet
{
  private SchemaSerializationMode _schemaSerializationMode = SchemaSerializationMode.IncludeSchema;
  private dsRestorder.RestorderDataTable tableRestorder;

  [DebuggerNonUserCode]
  public dsRestorder()
  {
    this.BeginInit();
    this.InitClass();
    CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
    base.Tables.CollectionChanged += changeEventHandler;
    base.Relations.CollectionChanged += changeEventHandler;
    this.EndInit();
  }

  [DebuggerNonUserCode]
  protected dsRestorder(SerializationInfo info, StreamingContext context)
    : base(info, context, false)
  {
    if (this.IsBinarySerialized(info, context))
    {
      this.InitVars(false);
      CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
      this.Tables.CollectionChanged += changeEventHandler;
      this.Relations.CollectionChanged += changeEventHandler;
    }
    else
    {
      string s = (string) info.GetValue("XmlSchema", typeof (string));
      if (this.DetermineSchemaSerializationMode(info, context) == SchemaSerializationMode.IncludeSchema)
      {
        DataSet dataSet = new DataSet();
        dataSet.ReadXmlSchema((XmlReader) new XmlTextReader((TextReader) new StringReader(s)));
        if (dataSet.Tables[nameof (Restorder)] != null)
          base.Tables.Add((DataTable) new dsRestorder.RestorderDataTable(dataSet.Tables[nameof (Restorder)]));
        this.DataSetName = dataSet.DataSetName;
        this.Prefix = dataSet.Prefix;
        this.Namespace = dataSet.Namespace;
        this.Locale = dataSet.Locale;
        this.CaseSensitive = dataSet.CaseSensitive;
        this.EnforceConstraints = dataSet.EnforceConstraints;
        this.Merge(dataSet, false, MissingSchemaAction.Add);
        this.InitVars();
      }
      else
        this.ReadXmlSchema((XmlReader) new XmlTextReader((TextReader) new StringReader(s)));
      this.GetSerializationData(info, context);
      CollectionChangeEventHandler changeEventHandler = new CollectionChangeEventHandler(this.SchemaChanged);
      base.Tables.CollectionChanged += changeEventHandler;
      this.Relations.CollectionChanged += changeEventHandler;
    }
  }

  [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
  [DebuggerNonUserCode]
  [Browsable(false)]
  public dsRestorder.RestorderDataTable Restorder
  {
    get
    {
      return this.tableRestorder;
    }
  }

  [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
  [DebuggerNonUserCode]
  [Browsable(true)]
  public override SchemaSerializationMode SchemaSerializationMode
  {
    get
    {
      return this._schemaSerializationMode;
    }
    set
    {
      this._schemaSerializationMode = value;
    }
  }

  [DebuggerNonUserCode]
  [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
  public new DataTableCollection Tables
  {
    get
    {
      return base.Tables;
    }
  }

  [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
  [DebuggerNonUserCode]
  public new DataRelationCollection Relations
  {
    get
    {
      return base.Relations;
    }
  }

  [DebuggerNonUserCode]
  protected override void InitializeDerivedDataSet()
  {
    this.BeginInit();
    this.InitClass();
    this.EndInit();
  }

  [DebuggerNonUserCode]
  public override DataSet Clone()
  {
    dsRestorder dsRestorder = (dsRestorder) base.Clone();
    dsRestorder.InitVars();
    dsRestorder.SchemaSerializationMode = this.SchemaSerializationMode;
    return (DataSet) dsRestorder;
  }

  [DebuggerNonUserCode]
  protected override bool ShouldSerializeTables()
  {
    return false;
  }

  [DebuggerNonUserCode]
  protected override bool ShouldSerializeRelations()
  {
    return false;
  }

  [DebuggerNonUserCode]
  protected override void ReadXmlSerializable(XmlReader reader)
  {
    if (this.DetermineSchemaSerializationMode(reader) == SchemaSerializationMode.IncludeSchema)
    {
      this.Reset();
      DataSet dataSet = new DataSet();
      int num = (int) dataSet.ReadXml(reader);
      if (dataSet.Tables["Restorder"] != null)
        base.Tables.Add((DataTable) new dsRestorder.RestorderDataTable(dataSet.Tables["Restorder"]));
      this.DataSetName = dataSet.DataSetName;
      this.Prefix = dataSet.Prefix;
      this.Namespace = dataSet.Namespace;
      this.Locale = dataSet.Locale;
      this.CaseSensitive = dataSet.CaseSensitive;
      this.EnforceConstraints = dataSet.EnforceConstraints;
      this.Merge(dataSet, false, MissingSchemaAction.Add);
      this.InitVars();
    }
    else
    {
      int num = (int) this.ReadXml(reader);
      this.InitVars();
    }
  }

  [DebuggerNonUserCode]
  protected override XmlSchema GetSchemaSerializable()
  {
    MemoryStream memoryStream = new MemoryStream();
    this.WriteXmlSchema((XmlWriter) new XmlTextWriter((Stream) memoryStream, (Encoding) null));
    memoryStream.Position = 0L;
    return XmlSchema.Read((XmlReader) new XmlTextReader((Stream) memoryStream), (ValidationEventHandler) null);
  }

  [DebuggerNonUserCode]
  internal void InitVars()
  {
    this.InitVars(true);
  }

  [DebuggerNonUserCode]
  internal void InitVars(bool initTable)
  {
    this.tableRestorder = (dsRestorder.RestorderDataTable) base.Tables["Restorder"];
    if (!initTable || this.tableRestorder == null)
      return;
    this.tableRestorder.InitVars();
  }

  [DebuggerNonUserCode]
  private void InitClass()
  {
    this.DataSetName = nameof (dsRestorder);
    this.Prefix = "";
    this.Namespace = "http://tempuri.org/dsRestorder.xsd";
    this.Locale = new CultureInfo("en");
    this.EnforceConstraints = true;
    this.SchemaSerializationMode = SchemaSerializationMode.IncludeSchema;
    this.tableRestorder = new dsRestorder.RestorderDataTable();
    base.Tables.Add((DataTable) this.tableRestorder);
  }

  [DebuggerNonUserCode]
  private bool ShouldSerializeRestorder()
  {
    return false;
  }

  [DebuggerNonUserCode]
  private void SchemaChanged(object sender, CollectionChangeEventArgs e)
  {
    if (e.Action != CollectionChangeAction.Remove)
      return;
    this.InitVars();
  }

  [DebuggerNonUserCode]
  public static XmlSchemaComplexType GetTypedDataSetSchema(XmlSchemaSet xs)
  {
    dsRestorder dsRestorder = new dsRestorder();
    XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
    XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
    xs.Add(dsRestorder.GetSchemaSerializable());
    xmlSchemaSequence.Items.Add((XmlSchemaObject) new XmlSchemaAny()
    {
      Namespace = dsRestorder.Namespace
    });
    schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
    return schemaComplexType;
  }

  public delegate void RestorderRowChangeEventHandler(object sender, dsRestorder.RestorderRowChangeEvent e);

  [XmlSchemaProvider("GetTypedTableSchema")]
  [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
  [Serializable]
  public class RestorderDataTable : DataTable, IEnumerable
  {
    private DataColumn columnOrderNr;
    private DataColumn columnOrderdatum;
    private DataColumn columnLeveranstid;
    private DataColumn columnSaljarNr;
    private DataColumn columnSaljarNamn;
    private DataColumn columnKundNr;
    private DataColumn columnKundNamn;
    private DataColumn columnRad;
    private DataColumn columnArtikelNr;
    private DataColumn columnArtikelBen;
    private DataColumn columnUrsprungligt_antal;
    private DataColumn columnLevererat_antal;
    private DataColumn columnOrderStatus;
    private DataColumn columnOrdersumma;
    private DataColumn columnSumma_olevererat;
    private DataColumn columnSumma_levererat;
    private DataColumn columnSumma_fakturerat;
    private DataColumn columnSumma_restbelopp;

    [DebuggerNonUserCode]
    public RestorderDataTable()
    {
      this.TableName = "Restorder";
      this.BeginInit();
      this.InitClass();
      this.EndInit();
    }

    [DebuggerNonUserCode]
    internal RestorderDataTable(DataTable table)
    {
      this.TableName = table.TableName;
      if (table.CaseSensitive != table.DataSet.CaseSensitive)
        this.CaseSensitive = table.CaseSensitive;
      if (table.Locale.ToString() != table.DataSet.Locale.ToString())
        this.Locale = table.Locale;
      if (table.Namespace != table.DataSet.Namespace)
        this.Namespace = table.Namespace;
      this.Prefix = table.Prefix;
      this.MinimumCapacity = table.MinimumCapacity;
    }

    [DebuggerNonUserCode]
    protected RestorderDataTable(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      this.InitVars();
    }

    [DebuggerNonUserCode]
    public DataColumn OrderNrColumn
    {
      get
      {
        return this.columnOrderNr;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn OrderdatumColumn
    {
      get
      {
        return this.columnOrderdatum;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn LeveranstidColumn
    {
      get
      {
        return this.columnLeveranstid;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn SaljarNrColumn
    {
      get
      {
        return this.columnSaljarNr;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn SaljarNamnColumn
    {
      get
      {
        return this.columnSaljarNamn;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn KundNrColumn
    {
      get
      {
        return this.columnKundNr;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn KundNamnColumn
    {
      get
      {
        return this.columnKundNamn;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn RadColumn
    {
      get
      {
        return this.columnRad;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn ArtikelNrColumn
    {
      get
      {
        return this.columnArtikelNr;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn ArtikelBenColumn
    {
      get
      {
        return this.columnArtikelBen;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn Ursprungligt_antalColumn
    {
      get
      {
        return this.columnUrsprungligt_antal;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn Levererat_antalColumn
    {
      get
      {
        return this.columnLevererat_antal;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn OrderStatusColumn
    {
      get
      {
        return this.columnOrderStatus;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn OrdersummaColumn
    {
      get
      {
        return this.columnOrdersumma;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn Summa_olevereratColumn
    {
      get
      {
        return this.columnSumma_olevererat;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn Summa_levereratColumn
    {
      get
      {
        return this.columnSumma_levererat;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn Summa_faktureratColumn
    {
      get
      {
        return this.columnSumma_fakturerat;
      }
    }

    [DebuggerNonUserCode]
    public DataColumn Summa_restbeloppColumn
    {
      get
      {
        return this.columnSumma_restbelopp;
      }
    }

    [DebuggerNonUserCode]
    [Browsable(false)]
    public int Count
    {
      get
      {
        return this.Rows.Count;
      }
    }

    [DebuggerNonUserCode]
    public dsRestorder.RestorderRow this[int index]
    {
      get
      {
        return (dsRestorder.RestorderRow) this.Rows[index];
      }
    }

    public event dsRestorder.RestorderRowChangeEventHandler RestorderRowChanging;

    public event dsRestorder.RestorderRowChangeEventHandler RestorderRowChanged;

    public event dsRestorder.RestorderRowChangeEventHandler RestorderRowDeleting;

    public event dsRestorder.RestorderRowChangeEventHandler RestorderRowDeleted;

    [DebuggerNonUserCode]
    public void AddRestorderRow(dsRestorder.RestorderRow row)
    {
      this.Rows.Add((DataRow) row);
    }

    [DebuggerNonUserCode]
    public dsRestorder.RestorderRow AddRestorderRow(string OrderNr, string Orderdatum, string Leveranstid, string SaljarNr, string SaljarNamn, string KundNr, string KundNamn, string Rad, string ArtikelNr, string ArtikelBen, double Ursprungligt_antal, double Levererat_antal, string OrderStatus, double Ordersumma, double Summa_olevererat, double Summa_levererat, double Summa_fakturerat, double Summa_restbelopp)
    {
      dsRestorder.RestorderRow restorderRow = (dsRestorder.RestorderRow) this.NewRow();
      restorderRow.ItemArray = new object[18]
      {
        (object) OrderNr,
        (object) Orderdatum,
        (object) Leveranstid,
        (object) SaljarNr,
        (object) SaljarNamn,
        (object) KundNr,
        (object) KundNamn,
        (object) Rad,
        (object) ArtikelNr,
        (object) ArtikelBen,
        (object) Ursprungligt_antal,
        (object) Levererat_antal,
        (object) OrderStatus,
        (object) Ordersumma,
        (object) Summa_olevererat,
        (object) Summa_levererat,
        (object) Summa_fakturerat,
        (object) Summa_restbelopp
      };
      this.Rows.Add((DataRow) restorderRow);
      return restorderRow;
    }

    [DebuggerNonUserCode]
    public virtual IEnumerator GetEnumerator()
    {
      return this.Rows.GetEnumerator();
    }

    [DebuggerNonUserCode]
    public override DataTable Clone()
    {
      dsRestorder.RestorderDataTable restorderDataTable = (dsRestorder.RestorderDataTable) base.Clone();
      restorderDataTable.InitVars();
      return (DataTable) restorderDataTable;
    }

    [DebuggerNonUserCode]
    protected override DataTable CreateInstance()
    {
      return (DataTable) new dsRestorder.RestorderDataTable();
    }

    [DebuggerNonUserCode]
    internal void InitVars()
    {
      this.columnOrderNr = this.Columns["OrderNr"];
      this.columnOrderdatum = this.Columns["Orderdatum"];
      this.columnLeveranstid = this.Columns["Leveranstid"];
      this.columnSaljarNr = this.Columns["SaljarNr"];
      this.columnSaljarNamn = this.Columns["SaljarNamn"];
      this.columnKundNr = this.Columns["KundNr"];
      this.columnKundNamn = this.Columns["KundNamn"];
      this.columnRad = this.Columns["Rad"];
      this.columnArtikelNr = this.Columns["ArtikelNr"];
      this.columnArtikelBen = this.Columns["ArtikelBen"];
      this.columnUrsprungligt_antal = this.Columns["Ursprungligt_antal"];
      this.columnLevererat_antal = this.Columns["Levererat_antal"];
      this.columnOrderStatus = this.Columns["OrderStatus"];
      this.columnOrdersumma = this.Columns["Ordersumma"];
      this.columnSumma_olevererat = this.Columns["Summa_olevererat"];
      this.columnSumma_levererat = this.Columns["Summa_levererat"];
      this.columnSumma_fakturerat = this.Columns["Summa_fakturerat"];
      this.columnSumma_restbelopp = this.Columns["Summa_restbelopp"];
    }

    [DebuggerNonUserCode]
    private void InitClass()
    {
      this.columnOrderNr = new DataColumn("OrderNr", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnOrderNr);
      this.columnOrderdatum = new DataColumn("Orderdatum", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnOrderdatum);
      this.columnLeveranstid = new DataColumn("Leveranstid", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnLeveranstid);
      this.columnSaljarNr = new DataColumn("SaljarNr", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnSaljarNr);
      this.columnSaljarNamn = new DataColumn("SaljarNamn", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnSaljarNamn);
      this.columnKundNr = new DataColumn("KundNr", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnKundNr);
      this.columnKundNamn = new DataColumn("KundNamn", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnKundNamn);
      this.columnRad = new DataColumn("Rad", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnRad);
      this.columnArtikelNr = new DataColumn("ArtikelNr", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnArtikelNr);
      this.columnArtikelBen = new DataColumn("ArtikelBen", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnArtikelBen);
      this.columnUrsprungligt_antal = new DataColumn("Ursprungligt_antal", typeof (double), (string) null, MappingType.Element);
      this.Columns.Add(this.columnUrsprungligt_antal);
      this.columnLevererat_antal = new DataColumn("Levererat_antal", typeof (double), (string) null, MappingType.Element);
      this.Columns.Add(this.columnLevererat_antal);
      this.columnOrderStatus = new DataColumn("OrderStatus", typeof (string), (string) null, MappingType.Element);
      this.Columns.Add(this.columnOrderStatus);
      this.columnOrdersumma = new DataColumn("Ordersumma", typeof (double), (string) null, MappingType.Element);
      this.Columns.Add(this.columnOrdersumma);
      this.columnSumma_olevererat = new DataColumn("Summa_olevererat", typeof (double), (string) null, MappingType.Element);
      this.Columns.Add(this.columnSumma_olevererat);
      this.columnSumma_levererat = new DataColumn("Summa_levererat", typeof (double), (string) null, MappingType.Element);
      this.Columns.Add(this.columnSumma_levererat);
      this.columnSumma_fakturerat = new DataColumn("Summa_fakturerat", typeof (double), (string) null, MappingType.Element);
      this.Columns.Add(this.columnSumma_fakturerat);
      this.columnSumma_restbelopp = new DataColumn("Summa_restbelopp", typeof (double), (string) null, MappingType.Element);
      this.Columns.Add(this.columnSumma_restbelopp);
      this.columnSaljarNr.MaxLength = 2;
      this.columnSaljarNamn.MaxLength = 35;
      this.columnKundNr.MaxLength = 10;
      this.columnArtikelNr.MaxLength = 13;
      this.Locale = new CultureInfo("en");
    }

    [DebuggerNonUserCode]
    public dsRestorder.RestorderRow NewRestorderRow()
    {
      return (dsRestorder.RestorderRow) this.NewRow();
    }

    [DebuggerNonUserCode]
    protected override DataRow NewRowFromBuilder(DataRowBuilder builder)
    {
      return (DataRow) new dsRestorder.RestorderRow(builder);
    }

    [DebuggerNonUserCode]
    protected override Type GetRowType()
    {
      return typeof (dsRestorder.RestorderRow);
    }

    [DebuggerNonUserCode]
    protected override void OnRowChanged(DataRowChangeEventArgs e)
    {
      base.OnRowChanged(e);
      if (this.RestorderRowChanged == null)
        return;
      this.RestorderRowChanged((object) this, new dsRestorder.RestorderRowChangeEvent((dsRestorder.RestorderRow) e.Row, e.Action));
    }

    [DebuggerNonUserCode]
    protected override void OnRowChanging(DataRowChangeEventArgs e)
    {
      base.OnRowChanging(e);
      if (this.RestorderRowChanging == null)
        return;
      this.RestorderRowChanging((object) this, new dsRestorder.RestorderRowChangeEvent((dsRestorder.RestorderRow) e.Row, e.Action));
    }

    [DebuggerNonUserCode]
    protected override void OnRowDeleted(DataRowChangeEventArgs e)
    {
      base.OnRowDeleted(e);
      if (this.RestorderRowDeleted == null)
        return;
      this.RestorderRowDeleted((object) this, new dsRestorder.RestorderRowChangeEvent((dsRestorder.RestorderRow) e.Row, e.Action));
    }

    [DebuggerNonUserCode]
    protected override void OnRowDeleting(DataRowChangeEventArgs e)
    {
      base.OnRowDeleting(e);
      if (this.RestorderRowDeleting == null)
        return;
      this.RestorderRowDeleting((object) this, new dsRestorder.RestorderRowChangeEvent((dsRestorder.RestorderRow) e.Row, e.Action));
    }

    [DebuggerNonUserCode]
    public void RemoveRestorderRow(dsRestorder.RestorderRow row)
    {
      this.Rows.Remove((DataRow) row);
    }

    [DebuggerNonUserCode]
    public static XmlSchemaComplexType GetTypedTableSchema(XmlSchemaSet xs)
    {
      XmlSchemaComplexType schemaComplexType = new XmlSchemaComplexType();
      XmlSchemaSequence xmlSchemaSequence = new XmlSchemaSequence();
      dsRestorder dsRestorder = new dsRestorder();
      xs.Add(dsRestorder.GetSchemaSerializable());
      XmlSchemaAny xmlSchemaAny1 = new XmlSchemaAny();
      xmlSchemaAny1.Namespace = "http://www.w3.org/2001/XMLSchema";
      xmlSchemaAny1.MinOccurs = new Decimal(0);
      xmlSchemaAny1.MaxOccurs = new Decimal(-1, -1, -1, false, (byte) 0);
      xmlSchemaAny1.ProcessContents = XmlSchemaContentProcessing.Lax;
      xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny1);
      XmlSchemaAny xmlSchemaAny2 = new XmlSchemaAny();
      xmlSchemaAny2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
      xmlSchemaAny2.MinOccurs = new Decimal(1);
      xmlSchemaAny2.ProcessContents = XmlSchemaContentProcessing.Lax;
      xmlSchemaSequence.Items.Add((XmlSchemaObject) xmlSchemaAny2);
      schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
      {
        Name = "namespace",
        FixedValue = dsRestorder.Namespace
      });
      schemaComplexType.Attributes.Add((XmlSchemaObject) new XmlSchemaAttribute()
      {
        Name = "tableTypeName",
        FixedValue = nameof (RestorderDataTable)
      });
      schemaComplexType.Particle = (XmlSchemaParticle) xmlSchemaSequence;
      return schemaComplexType;
    }
  }

  [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
  public class RestorderRow : DataRow
  {
    private dsRestorder.RestorderDataTable tableRestorder;

    [DebuggerNonUserCode]
    internal RestorderRow(DataRowBuilder rb)
      : base(rb)
    {
      this.tableRestorder = (dsRestorder.RestorderDataTable) this.Table;
    }

    [DebuggerNonUserCode]
    public string OrderNr
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.OrderNrColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'OrderNr' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.OrderNrColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string Orderdatum
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.OrderdatumColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Orderdatum' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.OrderdatumColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string Leveranstid
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.LeveranstidColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Leveranstid' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.LeveranstidColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string SaljarNr
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.SaljarNrColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'SaljarNr' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.SaljarNrColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string SaljarNamn
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.SaljarNamnColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'SaljarNamn' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.SaljarNamnColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string KundNr
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.KundNrColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'KundNr' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.KundNrColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string KundNamn
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.KundNamnColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'KundNamn' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.KundNamnColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string Rad
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.RadColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Rad' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.RadColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string ArtikelNr
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.ArtikelNrColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'ArtikelNr' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.ArtikelNrColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string ArtikelBen
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.ArtikelBenColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'ArtikelBen' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.ArtikelBenColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public double Ursprungligt_antal
    {
      get
      {
        try
        {
          return (double) this[this.tableRestorder.Ursprungligt_antalColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Ursprungligt_antal' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.Ursprungligt_antalColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public double Levererat_antal
    {
      get
      {
        try
        {
          return (double) this[this.tableRestorder.Levererat_antalColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Levererat_antal' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.Levererat_antalColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public string OrderStatus
    {
      get
      {
        try
        {
          return (string) this[this.tableRestorder.OrderStatusColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'OrderStatus' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.OrderStatusColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public double Ordersumma
    {
      get
      {
        try
        {
          return (double) this[this.tableRestorder.OrdersummaColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Ordersumma' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.OrdersummaColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public double Summa_olevererat
    {
      get
      {
        try
        {
          return (double) this[this.tableRestorder.Summa_olevereratColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Summa_olevererat' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.Summa_olevereratColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public double Summa_levererat
    {
      get
      {
        try
        {
          return (double) this[this.tableRestorder.Summa_levereratColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Summa_levererat' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.Summa_levereratColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public double Summa_fakturerat
    {
      get
      {
        try
        {
          return (double) this[this.tableRestorder.Summa_faktureratColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Summa_fakturerat' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.Summa_faktureratColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public double Summa_restbelopp
    {
      get
      {
        try
        {
          return (double) this[this.tableRestorder.Summa_restbeloppColumn];
        }
        catch (InvalidCastException ex)
        {
          throw new StrongTypingException("The value for column 'Summa_restbelopp' in table 'Restorder' is DBNull.", (Exception) ex);
        }
      }
      set
      {
        this[this.tableRestorder.Summa_restbeloppColumn] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    public bool IsOrderNrNull()
    {
      return this.IsNull(this.tableRestorder.OrderNrColumn);
    }

    [DebuggerNonUserCode]
    public void SetOrderNrNull()
    {
      this[this.tableRestorder.OrderNrColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsOrderdatumNull()
    {
      return this.IsNull(this.tableRestorder.OrderdatumColumn);
    }

    [DebuggerNonUserCode]
    public void SetOrderdatumNull()
    {
      this[this.tableRestorder.OrderdatumColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsLeveranstidNull()
    {
      return this.IsNull(this.tableRestorder.LeveranstidColumn);
    }

    [DebuggerNonUserCode]
    public void SetLeveranstidNull()
    {
      this[this.tableRestorder.LeveranstidColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsSaljarNrNull()
    {
      return this.IsNull(this.tableRestorder.SaljarNrColumn);
    }

    [DebuggerNonUserCode]
    public void SetSaljarNrNull()
    {
      this[this.tableRestorder.SaljarNrColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsSaljarNamnNull()
    {
      return this.IsNull(this.tableRestorder.SaljarNamnColumn);
    }

    [DebuggerNonUserCode]
    public void SetSaljarNamnNull()
    {
      this[this.tableRestorder.SaljarNamnColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsKundNrNull()
    {
      return this.IsNull(this.tableRestorder.KundNrColumn);
    }

    [DebuggerNonUserCode]
    public void SetKundNrNull()
    {
      this[this.tableRestorder.KundNrColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsKundNamnNull()
    {
      return this.IsNull(this.tableRestorder.KundNamnColumn);
    }

    [DebuggerNonUserCode]
    public void SetKundNamnNull()
    {
      this[this.tableRestorder.KundNamnColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsRadNull()
    {
      return this.IsNull(this.tableRestorder.RadColumn);
    }

    [DebuggerNonUserCode]
    public void SetRadNull()
    {
      this[this.tableRestorder.RadColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsArtikelNrNull()
    {
      return this.IsNull(this.tableRestorder.ArtikelNrColumn);
    }

    [DebuggerNonUserCode]
    public void SetArtikelNrNull()
    {
      this[this.tableRestorder.ArtikelNrColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsArtikelBenNull()
    {
      return this.IsNull(this.tableRestorder.ArtikelBenColumn);
    }

    [DebuggerNonUserCode]
    public void SetArtikelBenNull()
    {
      this[this.tableRestorder.ArtikelBenColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsUrsprungligt_antalNull()
    {
      return this.IsNull(this.tableRestorder.Ursprungligt_antalColumn);
    }

    [DebuggerNonUserCode]
    public void SetUrsprungligt_antalNull()
    {
      this[this.tableRestorder.Ursprungligt_antalColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsLevererat_antalNull()
    {
      return this.IsNull(this.tableRestorder.Levererat_antalColumn);
    }

    [DebuggerNonUserCode]
    public void SetLevererat_antalNull()
    {
      this[this.tableRestorder.Levererat_antalColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsOrderStatusNull()
    {
      return this.IsNull(this.tableRestorder.OrderStatusColumn);
    }

    [DebuggerNonUserCode]
    public void SetOrderStatusNull()
    {
      this[this.tableRestorder.OrderStatusColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsOrdersummaNull()
    {
      return this.IsNull(this.tableRestorder.OrdersummaColumn);
    }

    [DebuggerNonUserCode]
    public void SetOrdersummaNull()
    {
      this[this.tableRestorder.OrdersummaColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsSumma_olevereratNull()
    {
      return this.IsNull(this.tableRestorder.Summa_olevereratColumn);
    }

    [DebuggerNonUserCode]
    public void SetSumma_olevereratNull()
    {
      this[this.tableRestorder.Summa_olevereratColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsSumma_levereratNull()
    {
      return this.IsNull(this.tableRestorder.Summa_levereratColumn);
    }

    [DebuggerNonUserCode]
    public void SetSumma_levereratNull()
    {
      this[this.tableRestorder.Summa_levereratColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsSumma_faktureratNull()
    {
      return this.IsNull(this.tableRestorder.Summa_faktureratColumn);
    }

    [DebuggerNonUserCode]
    public void SetSumma_faktureratNull()
    {
      this[this.tableRestorder.Summa_faktureratColumn] = Convert.DBNull;
    }

    [DebuggerNonUserCode]
    public bool IsSumma_restbeloppNull()
    {
      return this.IsNull(this.tableRestorder.Summa_restbeloppColumn);
    }

    [DebuggerNonUserCode]
    public void SetSumma_restbeloppNull()
    {
      this[this.tableRestorder.Summa_restbeloppColumn] = Convert.DBNull;
    }
  }

  [GeneratedCode("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
  public class RestorderRowChangeEvent : EventArgs
  {
    private dsRestorder.RestorderRow eventRow;
    private DataRowAction eventAction;

    [DebuggerNonUserCode]
    public RestorderRowChangeEvent(dsRestorder.RestorderRow row, DataRowAction action)
    {
      this.eventRow = row;
      this.eventAction = action;
    }

    [DebuggerNonUserCode]
    public dsRestorder.RestorderRow Row
    {
      get
      {
        return this.eventRow;
      }
    }

    [DebuggerNonUserCode]
    public DataRowAction Action
    {
      get
      {
        return this.eventAction;
      }
    }
  }
}
