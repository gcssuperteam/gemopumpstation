﻿// Decompiled with JetBrains decompiler
// Type: GCS_PumpStation.Logger
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace GCS_PumpStation
{
  public static class Logger
  {
    private static string mPath = "";
    private static StreamWriter sw;

    private static void openLoggFile(string user)
    {
      string path = Application.StartupPath + "\\LoggFiles\\";
      Logger.mPath = Application.StartupPath + "\\LoggFiles\\logg_for_" + user + ".txt";
      try
      {
        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);
        Logger.sw = new StreamWriter(Logger.mPath, true, Encoding.Default);
      }
      catch (Exception ex)
      {
      }
    }

    private static void closeLoggFile()
    {
      try
      {
        Logger.sw.Close();
        Logger.sw = (StreamWriter) null;
        Logger.sw.Dispose();
        GC.Collect();
      }
      catch
      {
      }
    }

    public static void loggError(Exception e, string own_err_desc, string who)
    {
      Logger.openLoggFile("SYS");
      try
      {
        Logger.sw.WriteLine(DateTime.Now.ToString() + " | " + who + " | " + e.TargetSite.ToString() + " | " + e.Message + " | " + e.StackTrace.Replace("\n", "").Replace("\r", "") + " | " + own_err_desc);
      }
      catch
      {
        Logger.closeLoggFile();
      }
      Logger.closeLoggFile();
    }

    public static void loggEvent(string desc, string who)
    {
      Logger.openLoggFile("SYS");
      try
      {
        Logger.sw.WriteLine(DateTime.Now.ToString() + " | " + who + " | " + desc);
      }
      catch
      {
        Logger.closeLoggFile();
      }
      Logger.closeLoggFile();
    }

    public static string getFileName()
    {
      return Logger.mPath;
    }
  }
}
