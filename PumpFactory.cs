﻿// Decompiled with JetBrains decompiler
// Type: GCS_PumpStation.PumpFactory
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using Excido;

namespace GCS_PumpStation
{
  internal class PumpFactory
  {
    public IGCS_Pump getPump_Object(string pump, string config_path)
    {
      if (ECS.noNULL((object) pump).Equals("GPS_Gemo"))
        return (IGCS_Pump) new GPS_Gemo(config_path);
      return (IGCS_Pump) null;
    }
  }
}
