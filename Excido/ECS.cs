﻿// Decompiled with JetBrains decompiler
// Type: Excido.ECS
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using System;
using System.Globalization;

namespace Excido
{
  internal static class ECS
  {
    public static string decSep = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
    public static string GarpDecSep = ".";

    public static double stringToDouble(string s)
    {
      try
      {
        if (ECS.decSep.Equals("."))
          return double.Parse(s.Replace(",", "."));
        return double.Parse(s.Replace(".", ","));
      }
      catch
      {
        return 0.0;
      }
    }

    public static string doubleToString(double d, char delimiter)
    {
      try
      {
        if (delimiter.Equals('.'))
          return d.ToString().Replace(",", ".");
        return d.ToString().Replace(".", ",");
      }
      catch (Exception ex)
      {
        return "0";
      }
    }

    public static double roundDouble(double d, int dec)
    {
      return Math.Round(d, dec);
    }

    public static string roundString(string s, int dec)
    {
      s = ECS.doubleToString(ECS.roundDouble(ECS.stringToDouble(s), dec), ECS.decSep.ToCharArray()[0]);
      if (s.IndexOf(ECS.decSep) != -1)
      {
        for (int index = 0; index < dec - s.Split(ECS.decSep.ToCharArray())[1].Length; ++index)
          s += "0";
      }
      return s;
    }

    public static string setDecSepAsCulture(string s)
    {
      if (ECS.decSep.Equals("."))
        return s.Replace(",", ".");
      return s.Replace(".", ",");
    }

    public static string toGarpDecSep(string s)
    {
      return s.Replace(",", ".");
    }

    public static bool isNumeric(string s)
    {
      double result;
      return double.TryParse(s, out result);
    }

    public static string noNULL(object o)
    {
      if (o == null)
        return "";
      return (string) o;
    }
  }
}
