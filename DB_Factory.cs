﻿// Decompiled with JetBrains decompiler
// Type: GCS_PumpStation.DB_Factory
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using Excido;

namespace GCS_PumpStation
{
  internal class DB_Factory
  {
    public IDataBaseFunctions getDB_Object(string con_string, string type)
    {
      if (ECS.noNULL((object) type).Equals("XML"))
        return (IDataBaseFunctions) new XML_Db(con_string, type);
      return (IDataBaseFunctions) null;
    }
  }
}
