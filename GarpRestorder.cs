﻿// Decompiled with JetBrains decompiler
// Type: GCS_PumpStation.GarpRestorder
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using Excido;
using Garp;
using System.ComponentModel;
using System.Data;

namespace GCS_PumpStation
{
    public class GarpRestorder
    {
        private Application app;
        private ITable OGA;
        private ITable OGR;
        private ITable HKA;
        private ITable HKR;
        private ITable KA;
        private ITable TA04;
        private ITabField OGA_ONR;
        private ITabField OGA_KNR;
        private ITabField OGA_OTY;
        private ITabField OGA_ODT;
        private ITabField OGA_BLT;
        private ITabField OGA_SLJ;
        private ITabField OGR_ONR;
        private ITabField OGR_RDC;
        private ITabField OGR_ANR;
        private ITabField OGR_BEN;
        private ITabField OGR_LAG;
        private ITabField OGR_ORA;
        private ITabField OGR_TLA;
        private ITabField OGR_RAB;
        private ITabField OGR_LVF;
        private ITabField HKA_ONR;
        private ITabField HKA_HNR;
        private ITabField HKA_FSF;
        private ITabField HKR_HNR;
        private ITabField HKR_ONR;
        private ITabField HKR_RDC;
        private ITabField HKR_LEA;
        private ITabField HKR_PRI;
        private ITabField HKR_RAB;
        private ITabField HKR_FAF;
        private ITabField KA_NAM;
        private ITabField TA04_TX1;
        private string mDatefrom;
        private string mDateto;
        private string mSellerfrom;
        private string mSellerto;
        private string mXML_path;
        private BackgroundWorker mWorker;
        private DoWorkEventArgs mEvent;

        public GarpRestorder()
        {
            this.app = new Garp.Application();
            this.OGA = this.app.Tables.Item(nameof(OGA));
            this.OGR = this.app.Tables.Item(nameof(OGR));
            this.HKA = this.app.Tables.Item(nameof(HKA));
            this.HKR = this.app.Tables.Item(nameof(HKR));
            this.KA = this.app.Tables.Item(nameof(KA));
            this.TA04 = this.app.Tables.Item(nameof(TA04));
            this.OGA_ONR = this.OGA.Fields.Item("ONR");
            this.OGA_KNR = this.OGA.Fields.Item("KNR");
            this.OGA_OTY = this.OGA.Fields.Item("OTY");
            this.OGA_ODT = this.OGA.Fields.Item("ODT");
            this.OGA_SLJ = this.OGA.Fields.Item("SLJ");
            this.OGA_BLT = this.OGA.Fields.Item("BLT");
            this.OGR_ONR = this.OGR.Fields.Item("ONR");
            this.OGR_RDC = this.OGR.Fields.Item("RDC");
            this.OGR_ANR = this.OGR.Fields.Item("ANR");
            this.OGR_BEN = this.OGR.Fields.Item("BEN");
            this.OGR_LAG = this.OGR.Fields.Item("LAG");
            this.OGR_ORA = this.OGR.Fields.Item("ORA");
            this.OGR_TLA = this.OGR.Fields.Item("TLA");
            this.OGR_RAB = this.OGR.Fields.Item("RAB");
            this.OGR_LVF = this.OGR.Fields.Item("LVF");
            this.HKA_ONR = this.HKA.Fields.Item("ONR");
            this.HKA_HNR = this.HKA.Fields.Item("HNR");
            this.HKA_FSF = this.HKA.Fields.Item("FSF");
            this.HKR_HNR = this.HKR.Fields.Item("HNR");
            this.HKR_ONR = this.HKR.Fields.Item("ONR");
            this.HKR_RDC = this.HKR.Fields.Item("RDC");
            this.HKR_LEA = this.HKR.Fields.Item("LEA");
            this.HKR_PRI = this.HKR.Fields.Item("PRI");
            this.HKR_RAB = this.HKR.Fields.Item("RAB");
            this.HKR_FAF = this.HKR.Fields.Item("FAF");
            this.KA_NAM = this.KA.Fields.Item("NAM");
            this.TA04_TX1 = this.TA04.Fields.Item("TX1");
        }

        public int cleanRun()
        {
            this.OGA.Filter.AddField("OTY", false);
            this.OGA.Filter.AddConst("9", false, "C");
            this.OGA.Filter.AddConst("0", false, "C");
            this.OGA.Filter.Expression = "1e2&1e3";
            this.OGA.Filter.Active = true;
            this.OGA.First();
            int num = 0;
            while (!this.OGA.Eof)
            {
                ++num;
                this.OGA.Next();
            }
            return num;
        }

        public void selection()
        {
        }

        public void run(object[] selection)
        {
            this.mDatefrom = (string)selection[0];
            this.mDateto = (string)selection[1];
            this.mSellerfrom = (string)selection[2];
            this.mSellerto = (string)selection[3];
            this.mWorker = (BackgroundWorker)selection[4];
            this.mEvent = (DoWorkEventArgs)selection[5];
            DataSet dataSet = new DataSet();
            IOrderRowCalc orderRowCalc = this.app.OrderRowCalc;
            int num = (int)dataSet.ReadXml(this.mXML_path);
            dataSet.Clear();
            this.OGA.Filter.AddField("BLT", false);
            this.OGA.Filter.AddField("SLJ", false);
            this.OGA.Filter.AddField("OTY", false);
            this.OGA.Filter.AddField("RES", false);
            this.OGA.Filter.AddConst(this.mDatefrom, false, "C");
            this.OGA.Filter.AddConst(this.mDateto, false, "C");
            this.OGA.Filter.AddConst(this.mSellerfrom, false, "C");
            this.OGA.Filter.AddConst(this.mSellerto, false, "C");
            this.OGA.Filter.AddConst("9", false, "C");
            this.OGA.Filter.AddConst("0", false, "C");
            this.OGA.Filter.AddConst("1", false, "C");
            this.OGA.Filter.Expression = "(1n5&1m6)&(2n7&2m8)&(3e9&3eA)&4<B";
            this.OGA.Filter.Active = true;
            this.OGA.First();
            while (!this.OGA.Eof)
            {
                if (this.mWorker.CancellationPending)
                {
                    this.mEvent.Cancel = true;
                    return;
                }
                this.mWorker.ReportProgress(1, (object)this.OGA_ONR.Value);
                orderRowCalc.Ordernr = this.OGA_ONR.Value;
                orderRowCalc.RadnrFrom = "  1";
                orderRowCalc.RadnrTo = "250";
                orderRowCalc.Calculate();
                if (ECS.stringToDouble(orderRowCalc.UrsprVarde) > 0.0)
                {
                    this.KA.Find(this.OGA_KNR.Value);
                    this.TA04.Find(this.OGA_SLJ.Value);
                    this.OGR.Find(this.OGA_ONR.Value);
                    this.OGR.Next();
                    while (ECS.noNULL((object)this.OGA_ONR.Value).Equals(ECS.noNULL((object)this.OGR_ONR.Value)) & !this.OGR.Eof)
                    {
                        GarpRestorder.ResultFromCheck resultFromCheck = this.checkFS(this.OGA_ONR.Value, this.OGR_RDC.Value, this.OGR_LVF.Value);
                        orderRowCalc.Ordernr = this.OGA_ONR.Value;
                        orderRowCalc.RadnrFrom = this.OGR_RDC.Value;
                        orderRowCalc.RadnrTo = this.OGR_RDC.Value;
                        orderRowCalc.Calculate();
                        DataRow row = dataSet.Tables[0].NewRow();
                        row["OrderNr"] = (object)this.OGR_ONR.Value;
                        row["Orderdatum"] = (object)this.OGA_ODT.Value;
                        row["SaljarNr"] = (object)this.OGA_SLJ.Value;
                        row["SaljarNamn"] = (object)this.TA04_TX1.Value;
                        row["KundNr"] = (object)this.OGA_KNR.Value;
                        row["KundNamn"] = (object)this.KA_NAM.Value;
                        row["Rad"] = (object)this.OGR_RDC.Value;
                        row["ArtikelNr"] = (object)this.OGR_ANR.Value;
                        row["ArtikelBen"] = (object)this.OGR_BEN.Value;
                        row["OrderStatus"] = (object)resultFromCheck.Status;
                        row["Leveranstid"] = (object)this.OGA_BLT.Value;
                        row["Ursprungligt_antal"] = (object)ECS.stringToDouble(this.OGR_ORA.Value);
                        row["Levererat_antal"] = (object)ECS.stringToDouble(this.OGR_TLA.Value);
                        row["Ordersumma"] = (object)ECS.stringToDouble(orderRowCalc.UrsprVarde);
                        row["Summa_olevererat"] = (object)ECS.stringToDouble(orderRowCalc.ExklMoms);
                        row["Summa_levererat"] = (object)(ECS.stringToDouble(orderRowCalc.UrsprVarde) - ECS.stringToDouble(orderRowCalc.ExklMoms));
                        row["Summa_fakturerat"] = (object)resultFromCheck.SummaFakturerat;
                        row["Summa_restbelopp"] = (object)(double.Parse(row["Ordersumma"].ToString()) - resultFromCheck.SummaFakturerat);
                        if (ECS.stringToDouble(orderRowCalc.UrsprVarde) >= 0.0)
                        {
                            if (resultFromCheck.SummaUnderPlock + resultFromCheck.SummaLevererat > resultFromCheck.SummaFakturerat || !ECS.noNULL((object)this.OGR_LVF.Value).Equals("5"))
                                dataSet.Tables[0].Rows.Add(row);
                        }
                        else if (resultFromCheck.SummaUnderPlock + resultFromCheck.SummaLevererat < resultFromCheck.SummaFakturerat || !ECS.noNULL((object)this.OGR_LVF.Value).Equals("5"))
                            dataSet.Tables[0].Rows.Add(row);
                        this.OGR.Next();
                    }
                }
                this.OGA.Next();
            }
            dataSet.WriteXml(this.mXML_path, XmlWriteMode.WriteSchema);
            this.OGA.Filter.Clear();
        }

        private GarpRestorder.ResultFromCheck checkFS(string onr, string row, string OR_levflagga)
        {
            GarpRestorder.ResultFromCheck resultFromCheck = new GarpRestorder.ResultFromCheck();
            string str = "";
            resultFromCheck.SummaFakturerat = 0.0;
            resultFromCheck.SummaLevererat = 0.0;
            resultFromCheck.SummaUnderPlock = 0.0;
            this.HKA.IndexNo = 2;
            this.HKA.Find(onr.PadRight(6));
            this.HKA.Next();
            while (ECS.noNULL((object)this.HKA_ONR.Value).Trim().Equals(onr.Trim()) && !this.HKA.Eof)
            {
                if (this.HKR.Find(this.HKA_HNR.Value + "  1" + row.PadLeft(3)))
                {
                    double num1 = ECS.stringToDouble(this.HKR_LEA.Value) * ECS.stringToDouble(this.HKR_PRI.Value);
                    double num2 = num1 * (ECS.stringToDouble(this.HKR_RAB.Value) / 100.0);
                    if (ECS.noNULL((object)this.HKA_FSF.Value).Equals("5") && !ECS.noNULL((object)this.HKR_FAF.Value).Equals("5"))
                    {
                        resultFromCheck.SummaLevererat += num1 - num2;
                        resultFromCheck.Status += "L";
                    }
                    else if (!ECS.noNULL((object)this.HKA_FSF.Value).Equals("5") && !ECS.noNULL((object)this.HKR_FAF.Value).Equals("5"))
                    {
                        resultFromCheck.SummaUnderPlock += num1 - num2;
                        resultFromCheck.Status += "P";
                    }
                    else if (ECS.noNULL((object)this.HKR_FAF.Value).Equals("5"))
                    {
                        if (num1 > 0.0)
                            resultFromCheck.SummaFakturerat += num1 - num2;
                        else
                            resultFromCheck.SummaFakturerat += num1 + num2;
                        resultFromCheck.Status += "F";
                    }
                }
                this.HKA.Next();
            }
            if (str.Equals("") && ECS.noNULL((object)OR_levflagga).Equals("0"))
            {
                resultFromCheck.Status += "O";
            }
            return resultFromCheck;
        }

        public string getUser()
        {
            return this.app.User;
        }

        private struct ResultFromCheck
        {
            public string Status;
            public double SummaFakturerat;
            public double SummaUnderPlock;
            public double SummaLevererat;
        }
    }
}
