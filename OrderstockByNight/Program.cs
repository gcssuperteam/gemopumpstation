﻿// Decompiled with JetBrains decompiler
// Type: OrderstockByNight.Program
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using GCS_PumpStation;
using System;

namespace OrderstockByNight
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            PumpFactory pumpFactory = new PumpFactory();
            if (args.Length <= 1)
                return;
            try
            {
                pumpFactory.getPump_Object(args[0], args[1]).run(args[2]);
            }
            catch (Exception ex)
            {
                Logger.loggError(ex, "Errorhandler in main()", "");
            }
        }
    }
}
