﻿// Decompiled with JetBrains decompiler
// Type: GCS_PumpStation.GCS_Pump
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using System.Collections;
using System.Data;
using System.Xml;

namespace GCS_PumpStation
{
  public abstract class GCS_Pump : IGCS_Pump
  {
    private string mDB_Type;
    private string mDB_Location;
    private string mDB_DefinitionPath;
    private string mDB_ConnectionsString;
    private string mGarpUser;
    private string mGarpPassword;

    public abstract void run(string selection_path);

    public string getXSD_definitionPath()
    {
      return this.mDB_DefinitionPath;
    }

    public string getDB_Type()
    {
      return this.mDB_Type;
    }

    public string getDB_LocationPath()
    {
      return this.mDB_Location;
    }

    public string getDB_ConnectionString()
    {
      return this.mDB_ConnectionsString;
    }

    public string getGarp_User()
    {
      return this.mGarpUser;
    }

    public string getGarp_Password()
    {
      return this.mGarpPassword;
    }

    public void readConfig(string path)
    {
      XmlDocument xmlDocument = new XmlDocument();
      try
      {
        xmlDocument.Load(path);
        XmlNode xmlNode = (XmlNode) xmlDocument["Config"];
        this.mDB_Type = xmlNode["DestDB"].Attributes["Type"].InnerText;
        this.mDB_Location = xmlNode["DestDB"].Attributes["Path"].InnerText;
        this.mDB_DefinitionPath = xmlNode["DestDB"].Attributes["XSD_Path"].InnerText;
        this.mDB_ConnectionsString = xmlNode["DestDB"].Attributes["ConnectionString"].InnerText;
        this.mGarpUser = xmlNode["Garp"].Attributes["User"].InnerText;
        this.mGarpPassword = xmlNode["Garp"].Attributes["Password"].InnerText;
      }
      catch
      {
      }
    }

    public GCS_Pump.Selection[] readSelection(string path)
    {
      XmlDocument xmlDocument = new XmlDocument();
      GCS_Pump.Selection selection = new GCS_Pump.Selection();
      ArrayList arrayList = new ArrayList();
      try
      {
        xmlDocument.Load(path);
        foreach (XmlNode xmlNode in (XmlNode) xmlDocument["Selection"])
        {
          selection.Description = xmlNode.Attributes["Description"].Value;
          selection.Table = xmlNode.Attributes["Table"].Value;
          selection.Field = xmlNode.Attributes["Field"].Value;
          selection.Value = xmlNode.Attributes["Value"].Value;
          selection.Operand = xmlNode.Attributes["Operand"].Value;
          arrayList.Add((object) selection);
        }
      }
      catch
      {
        return (GCS_Pump.Selection[]) null;
      }
      return (GCS_Pump.Selection[]) arrayList.ToArray(typeof (GCS_Pump.Selection));
    }

    public virtual DataSet getDataSetByDefenition()
    {
      DataSet dataSet = new DataSet();
      dataSet.ReadXmlSchema(this.mDB_DefinitionPath);
      return dataSet;
    }

    public struct Selection
    {
      public string Description;
      public string Table;
      public string Field;
      public string Value;
      public string Operand;
    }
  }
}
