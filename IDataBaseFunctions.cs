﻿// Decompiled with JetBrains decompiler
// Type: GCS_PumpStation.IDataBaseFunctions
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using System.Data;

namespace GCS_PumpStation
{
  internal interface IDataBaseFunctions
  {
    void postDataSet(DataSet ds);

    void postSQL(string sql);

    void setConnectionString(string con_string, string type_of_connection);
  }
}
