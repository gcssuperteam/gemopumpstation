﻿// Decompiled with JetBrains decompiler
// Type: GCS_PumpStation.IGCS_Pump
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using System.Data;

namespace GCS_PumpStation
{
  internal interface IGCS_Pump
  {
    void run(string selection_path);

    string getXSD_definitionPath();

    string getDB_Type();

    string getDB_LocationPath();

    string getDB_ConnectionString();

    void readConfig(string path);

    DataSet getDataSetByDefenition();
  }
}
