﻿// Decompiled with JetBrains decompiler
// Type: GCS_PumpStation.DataBaseFunctions
// Assembly: GCS_PumpStation, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 3EAA870D-AED4-4FA5-A60B-7E6A76E650F3
// Assembly location: C:\Source\Gemo\GCS_PumpStation.exe

using Excido;
using System.Data;

namespace GCS_PumpStation
{
  public abstract class DataBaseFunctions : IDataBaseFunctions
  {
    protected string mConstring;
    protected string mTypeOfConnection;

    public DataBaseFunctions()
    {
    }

    public DataBaseFunctions(string con_string, string type_of_connection)
    {
      if (ECS.noNULL((object) this.mTypeOfConnection).Equals("XML"))
      {
        this.mConstring = con_string;
        this.mTypeOfConnection = type_of_connection;
      }
      else if (ECS.noNULL((object) this.mTypeOfConnection).Equals("SQL"))
      {
        this.mConstring = con_string;
        this.mTypeOfConnection = type_of_connection;
      }
      else
      {
        if (!ECS.noNULL((object) this.mTypeOfConnection).Equals("MYSQL"))
          return;
        this.mConstring = con_string;
        this.mTypeOfConnection = type_of_connection;
      }
    }

    public void postDataSet(DataSet ds)
    {
      ds.WriteXml(this.mConstring, XmlWriteMode.WriteSchema);
    }

    public virtual void postSQL(string sql)
    {
    }

    public virtual void setConnectionString(string con_string, string type_of_connection)
    {
      if (ECS.noNULL((object) type_of_connection).Equals("XML"))
      {
        this.mConstring = con_string;
        this.mTypeOfConnection = type_of_connection;
      }
      else if (ECS.noNULL((object) type_of_connection).Equals("SQL"))
      {
        this.mConstring = con_string;
        this.mTypeOfConnection = type_of_connection;
      }
      else
      {
        if (!ECS.noNULL((object) type_of_connection).Equals("MYSQL"))
          return;
        this.mConstring = con_string;
        this.mTypeOfConnection = type_of_connection;
      }
    }
  }
}
